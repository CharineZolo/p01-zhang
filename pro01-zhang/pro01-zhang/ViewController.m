//
//  ViewController.m
//  pro01-zhang
//
//  Created by Jing Zhang on 1/19/17.
//  Copyright © 2017 Jing Zhang. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    self.hellolabel.adjustsFontSizeToFitWidth = YES;
   
    
    // Dispose of any resources that can be recreated.
}


- (IBAction)HelloButton:(id)sender {
    
    self.hellolabel.text=@"Jing Zhang 's first assignment.";}
@end
