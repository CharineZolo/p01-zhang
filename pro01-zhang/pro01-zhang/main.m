//
//  main.m
//  pro01-zhang
//
//  Created by Jing Zhang on 1/19/17.
//  Copyright © 2017 Jing Zhang. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
