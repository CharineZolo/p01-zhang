//
//  AppDelegate.h
//  pro01-zhang
//
//  Created by Jing Zhang on 1/19/17.
//  Copyright © 2017 Jing Zhang. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

